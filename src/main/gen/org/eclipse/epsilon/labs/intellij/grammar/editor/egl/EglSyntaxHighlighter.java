// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.editor.egl;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.tree.IElementType;
import org.eclipse.epsilon.labs.intellij.grammar.editor.eol.EolSyntaxHighlighter;
import org.eclipse.epsilon.labs.intellij.grammar.parser.egl.EglLexerAdapter;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.EglTypes;
import org.jetbrains.annotations.NotNull;

public class EglSyntaxHighlighter extends EolSyntaxHighlighter {

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new EglLexerAdapter();
	}

	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
/* protected region superTokens on begin */
    return super.getTokenHighlights(tokenType);
/* protected region superTokens end */
		
	}
}
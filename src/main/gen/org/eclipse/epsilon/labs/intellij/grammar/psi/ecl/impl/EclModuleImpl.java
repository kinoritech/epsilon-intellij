/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.ecl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import org.eclipse.epsilon.labs.intellij.grammar.psi.EpsilonModuleImpl;
import org.eclipse.epsilon.labs.intellij.grammar.psi.ecl.*;
import org.eclipse.epsilon.labs.intellij.grammar.parser.ecl.EclParserUtil;

public class EclModuleImpl extends EpsilonModuleImpl implements EclModule {

  public EclModuleImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EclVisitor visitor) {
    visitor.visitModule(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EclVisitor) accept((EclVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EclContent getContent() {
    return findNotNullChildByClass(EclContent.class);
  }

}

// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.editor.egl;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import icons.EpsilonIcons;
import org.eclipse.epsilon.labs.intellij.grammar.editor.eol.EolColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EglColorSettingsPage extends EolColorSettingsPage {

    @Nullable
    @Override
    public Icon getIcon() {
        return EpsilonIcons.EGL_FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new EglSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "This is a demo EGL file\n" +
               "<h1>Book [%=index%]: [%=book.a_title%]</h1>\n" +
               "\n" +
               "<h2>Authors</h2>\n" +
               "<ul>\n" +
               "[%for (author in book.c_author) { %]\n" +
               "  <li>[%=author.text%]\n" +
               "[%}%]\n" +
               "</ul>";

    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "EGL";
    }
}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.erl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import org.eclipse.epsilon.labs.intellij.grammar.psi.eol.impl.EolStatementImpl;
import org.eclipse.epsilon.labs.intellij.grammar.psi.erl.*;

public class ErlStatementImpl extends EolStatementImpl implements ErlStatement {

  public ErlStatementImpl(ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ErlVisitor visitor) {
    visitor.visitStatement(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ErlVisitor) accept((ErlVisitor)visitor);
    else super.accept(visitor);
  }

}

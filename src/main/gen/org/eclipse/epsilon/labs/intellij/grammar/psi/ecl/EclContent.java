/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.ecl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EclContent extends PsiElement {

  @NotNull
  List<EclMatchRule> getMatchRuleList();

  @NotNull
  List<EclPost> getPostList();

  @NotNull
  List<EclPre> getPreList();

  //WARNING: getAnnotationBlockList(...) is skipped
  //matching getAnnotationBlockList(EclContent, ...)
  //methods are not found in EclParserUtil

  //WARNING: getOperationDeclarationList(...) is skipped
  //matching getOperationDeclarationList(EclContent, ...)
  //methods are not found in EclParserUtil

}

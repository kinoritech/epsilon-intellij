/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.evl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.evl.*;

public class EvlContentImpl extends ASTWrapperPsiElement implements EvlContent {

  public EvlContentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EvlVisitor visitor) {
    visitor.visitContent(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EvlVisitor) accept((EvlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EvlConstraint> getConstraintList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EvlConstraint.class);
  }

  @Override
  @NotNull
  public List<EvlContext> getContextList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EvlContext.class);
  }

  @Override
  @NotNull
  public List<EvlCritique> getCritiqueList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EvlCritique.class);
  }

  @Override
  @NotNull
  public List<EvlPost> getPostList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EvlPost.class);
  }

  @Override
  @NotNull
  public List<EvlPre> getPreList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EvlPre.class);
  }

}

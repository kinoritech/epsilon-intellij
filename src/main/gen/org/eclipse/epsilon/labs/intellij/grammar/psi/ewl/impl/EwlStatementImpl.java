/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.ewl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.ewl.*;

public class EwlStatementImpl extends ASTWrapperPsiElement implements EwlStatement {

  public EwlStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EwlVisitor visitor) {
    visitor.visitStatement(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EwlVisitor) accept((EwlVisitor)visitor);
    else super.accept(visitor);
  }

}

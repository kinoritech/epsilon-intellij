/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.egl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.*;

public class EglPrimitiveExprImpl extends EglExprImpl implements EglPrimitiveExpr {

  public EglPrimitiveExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitPrimitiveExpr(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglCollectionType getCollectionType() {
    return findChildByClass(EglCollectionType.class);
  }

  @Override
  @Nullable
  public EglExpressionInBrackets getExpressionInBrackets() {
    return findChildByClass(EglExpressionInBrackets.class);
  }

  @Override
  @Nullable
  public EglFeatureCall getFeatureCall() {
    return findChildByClass(EglFeatureCall.class);
  }

  @Override
  @Nullable
  public EglLiteral getLiteral() {
    return findChildByClass(EglLiteral.class);
  }

  @Override
  @Nullable
  public EglLiteralMapCollection getLiteralMapCollection() {
    return findChildByClass(EglLiteralMapCollection.class);
  }

  @Override
  @Nullable
  public EglLiteralSequentialCollection getLiteralSequentialCollection() {
    return findChildByClass(EglLiteralSequentialCollection.class);
  }

  @Override
  @Nullable
  public EglNativeType getNativeType() {
    return findChildByClass(EglNativeType.class);
  }

  @Override
  @Nullable
  public EglPathName getPathName() {
    return findChildByClass(EglPathName.class);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.evl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EvlFixBlock extends PsiElement {

  @NotNull
  EvlFixBody getFixBody();

  @Nullable
  EvlGuard getGuard();

  @NotNull
  EvlTitle getTitle();

  //WARNING: getLeftBrace(...) is skipped
  //matching getLeftBrace(EvlFixBlock, ...)
  //methods are not found in EvlParserUtil

  //WARNING: getGuardKey(...) is skipped
  //matching getGuardKey(EvlFixBlock, ...)
  //methods are not found in EvlParserUtil

  //WARNING: getRightBrace(...) is skipped
  //matching getRightBrace(EvlFixBlock, ...)
  //methods are not found in EvlParserUtil

}

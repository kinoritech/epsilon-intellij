/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.evl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.evl.*;

public class EvlFixBlockImpl extends ASTWrapperPsiElement implements EvlFixBlock {

  public EvlFixBlockImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EvlVisitor visitor) {
    visitor.visitFixBlock(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EvlVisitor) accept((EvlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EvlFixBody getFixBody() {
    return findNotNullChildByClass(EvlFixBody.class);
  }

  @Override
  @Nullable
  public EvlGuard getGuard() {
    return findChildByClass(EvlGuard.class);
  }

  @Override
  @NotNull
  public EvlTitle getTitle() {
    return findNotNullChildByClass(EvlTitle.class);
  }

}

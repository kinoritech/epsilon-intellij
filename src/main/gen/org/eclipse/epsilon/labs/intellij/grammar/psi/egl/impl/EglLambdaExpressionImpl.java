/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.egl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;

import static org.eclipse.epsilon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.*;

public class EglLambdaExpressionImpl extends ASTWrapperPsiElement implements EglLambdaExpression {

  public EglLambdaExpressionImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitLambdaExpression(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EglExpr getExpr() {
    return findNotNullChildByClass(EglExpr.class);
  }

  @Override
  @Nullable
  public EglFormalParameterList getFormalParameterList() {
    return findChildByClass(EglFormalParameterList.class);
  }

  @Override
  @Nullable
  public PsiElement getInOp() {
    return findChildByType(EGL_IN_OP);
  }

  @Override
  @Nullable
  public PsiElement getThenOp() {
    return findChildByType(EGL_THEN_OP);
  }

}

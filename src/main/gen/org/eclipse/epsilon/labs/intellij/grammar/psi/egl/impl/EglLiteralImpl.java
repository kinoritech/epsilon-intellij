/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.egl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;

import static org.eclipse.epsilon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.*;

public class EglLiteralImpl extends ASTWrapperPsiElement implements EglLiteral {

  public EglLiteralImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitLiteral(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglBoolean getBoolean() {
    return findChildByClass(EglBoolean.class);
  }

  @Override
  @Nullable
  public PsiElement getFloat() {
    return findChildByType(EGL_FLOAT);
  }

  @Override
  @Nullable
  public PsiElement getHasMoreBin() {
    return findChildByType(EGL_HAS_MORE_BIN);
  }

  @Override
  @Nullable
  public PsiElement getLoopCntBin() {
    return findChildByType(EGL_LOOP_CNT_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSelfBin() {
    return findChildByType(EGL_SELF_BIN);
  }

  @Override
  @Nullable
  public PsiElement getString() {
    return findChildByType(EGL_STRING);
  }

}

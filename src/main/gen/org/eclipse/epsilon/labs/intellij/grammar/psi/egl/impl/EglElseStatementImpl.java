/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.egl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;

import static org.eclipse.epsilon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.*;

public class EglElseStatementImpl extends ASTWrapperPsiElement implements EglElseStatement {

  public EglElseStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitElseStatement(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglStatement getStatement() {
    return findChildByClass(EglStatement.class);
  }

  @Override
  @Nullable
  public EglStatementBlock getStatementBlock() {
    return findChildByClass(EglStatementBlock.class);
  }

  @Override
  @NotNull
  public PsiElement getElseKey() {
    return findNotNullChildByType(EGL_ELSE_KEY);
  }

}

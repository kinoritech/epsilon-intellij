/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.*;

public class EglModuleImpl extends ASTWrapperPsiElement implements EglModule {

  public EglModuleImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitModule(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EglContent getContent() {
    return findNotNullChildByClass(EglContent.class);
  }

  @Override
  @NotNull
  public List<EglImportStatement> getImportStatementList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EglImportStatement.class);
  }

  @Override
  @NotNull
  public List<EglModelDeclaration> getModelDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EglModelDeclaration.class);
  }

}

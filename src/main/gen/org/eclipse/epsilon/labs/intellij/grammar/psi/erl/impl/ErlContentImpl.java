/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.erl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.erl.*;

public class ErlContentImpl extends ASTWrapperPsiElement implements ErlContent {

  public ErlContentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ErlVisitor visitor) {
    visitor.visitContent(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ErlVisitor) accept((ErlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ErlPost> getPostList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ErlPost.class);
  }

  @Override
  @NotNull
  public List<ErlPre> getPreList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ErlPre.class);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.evl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;

import static org.eclipse.epsilon.labs.intellij.grammar.psi.evl.EvlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.evl.*;

public class EvlConstraintImpl extends ASTWrapperPsiElement implements EvlConstraint {

  public EvlConstraintImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EvlVisitor visitor) {
    visitor.visitConstraint(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EvlVisitor) accept((EvlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EvlInvariantBlock getInvariantBlock() {
    return findNotNullChildByClass(EvlInvariantBlock.class);
  }

  @Override
  @NotNull
  public PsiElement getConstraintKey() {
    return findNotNullChildByType(EVL_CONSTRAINT_KEY);
  }

}

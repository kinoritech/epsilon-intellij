/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.ewl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EwlWizard extends PsiElement {

  @Nullable
  EwlDoBlock getDoBlock();

  @Nullable
  EwlGuard getGuard();

  @Nullable
  EwlTitleBlock getTitleBlock();

  @NotNull
  PsiElement getWizardKey();

  //WARNING: getLeftBrace(...) is skipped
  //matching getLeftBrace(EwlWizard, ...)
  //methods are not found in EwlParserUtil

}

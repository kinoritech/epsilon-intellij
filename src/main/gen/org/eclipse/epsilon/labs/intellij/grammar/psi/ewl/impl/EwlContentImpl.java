/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.ewl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epsilon.labs.intellij.grammar.psi.ewl.*;

public class EwlContentImpl extends ASTWrapperPsiElement implements EwlContent {

  public EwlContentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EwlVisitor visitor) {
    visitor.visitContent(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EwlVisitor) accept((EwlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EwlPost> getPostList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EwlPost.class);
  }

  @Override
  @NotNull
  public List<EwlPre> getPreList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EwlPre.class);
  }

  @Override
  @NotNull
  public List<EwlWizard> getWizardList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EwlWizard.class);
  }

}

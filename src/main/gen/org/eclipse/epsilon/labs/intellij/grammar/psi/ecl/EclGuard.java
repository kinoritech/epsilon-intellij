/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.ecl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EclGuard extends PsiElement {

  //WARNING: getExpr(...) is skipped
  //matching getExpr(EclGuard, ...)
  //methods are not found in EclParserUtil

  //WARNING: getStatementBlock(...) is skipped
  //matching getStatementBlock(EclGuard, ...)
  //methods are not found in EclParserUtil

}

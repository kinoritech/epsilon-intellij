/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.egl.impl;

import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;

import static org.eclipse.epsilon.labs.intellij.grammar.psi.egl.EglTypes.*;
import org.eclipse.epsilon.labs.intellij.grammar.psi.egl.*;

public class EglNewExprImpl extends EglExprImpl implements EglNewExpr {

  public EglNewExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitNewExpr(this);
  }

  @Override
  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglParameterList getParameterList() {
    return findChildByClass(EglParameterList.class);
  }

  @Override
  @NotNull
  public EglTypeName getTypeName() {
    return findNotNullChildByClass(EglTypeName.class);
  }

  @Override
  @NotNull
  public PsiElement getNewKey() {
    return findNotNullChildByType(EGL_NEW_KEY);
  }

}

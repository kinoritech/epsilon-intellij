/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epsilon.labs.intellij.grammar.psi.evl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EvlInvariantBlock extends PsiElement {

  @NotNull
  EvlCheck getCheck();

  @NotNull
  List<EvlFix> getFixList();

  @Nullable
  EvlGuard getGuard();

  @Nullable
  EvlMessage getMessage();

  //WARNING: getLeftBrace(...) is skipped
  //matching getLeftBrace(EvlInvariantBlock, ...)
  //methods are not found in EvlParserUtil

  //WARNING: getGuardKey(...) is skipped
  //matching getGuardKey(EvlInvariantBlock, ...)
  //methods are not found in EvlParserUtil

  //WARNING: getRightBrace(...) is skipped
  //matching getRightBrace(EvlInvariantBlock, ...)
  //methods are not found in EvlParserUtil

}

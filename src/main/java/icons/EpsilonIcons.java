/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public interface EpsilonIcons {
	Icon EOL_FILE = IconLoader.getIcon("eol.sgv", EpsilonIcons.class);
	Icon EVL_FILE = IconLoader.getIcon("evl.svg", EpsilonIcons.class);
	Icon ETL_FILE = IconLoader.getIcon("etl.svg", EpsilonIcons.class);
	Icon EWL_FILE = IconLoader.getIcon("ewl.svg", EpsilonIcons.class);
	Icon ECL_FILE = IconLoader.getIcon("ecl.svg", EpsilonIcons.class);
	Icon EGL_FILE = IconLoader.getIcon("egl.svg", EpsilonIcons.class);
}
